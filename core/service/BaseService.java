package com.metahorizon.laliguras.core.service;

import com.metahorizon.laliguras.core.enums.GRADE;

import java.util.List;


public interface BaseService<T> {
    T save(T t);

    List<T> findAll();

    void delete(Long id);

    List<T> findBy(GRADE grade, String subject);
    List<T>findBy(GRADE grade);

}
