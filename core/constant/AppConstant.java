package com.metahorizon.laliguras.core.constant;


public class AppConstant {

    public static final String DEFAULT_PASSWORD = "123456";
    public static final String MSG = "msg";
    public static final String INSERT_SUCCESS_MSG = "Record of %s having name %s Successfully saved on DB.";

    public static final int RECORD_ROW_ON_PAGE = 1;

    private AppConstant() {
    }
}
