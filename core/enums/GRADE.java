package com.metahorizon.laliguras.core.enums;


public enum GRADE {
    CLASS_1, CLASS_2, CLASS_3, CLASS_4, CLASS_5;

    public static GRADE getEnumFromString(String string) {
        GRADE[] grades = GRADE.values();
        for (GRADE grade : grades) {
            if (grade.toString().equals(string)) {
                return grade;
            }
        }
        throw new RuntimeException("No matching enum found for " + string);
    }
}
