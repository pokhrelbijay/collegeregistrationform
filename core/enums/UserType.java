package com.metahorizon.laliguras.core.enums;


public enum UserType {

    ADMIN, TEACHER, STUDENT
}
