package com.metahorizon.laliguras.user.controller;

import com.metahorizon.laliguras.core.constant.AppConstant;
import com.metahorizon.laliguras.core.enums.GRADE;
import com.metahorizon.laliguras.core.enums.STATE;
import com.metahorizon.laliguras.core.enums.UserType;
import com.metahorizon.laliguras.user.entity.Teacher;
import com.metahorizon.laliguras.user.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.thymeleaf.util.StringUtils;

import java.util.List;

@Controller
@RequestMapping("/teacher")
public class TeacherController {

    @Autowired
    private TeacherService teacherService;

    @GetMapping("/register")
    public String getTeacherForm(ModelMap map, @ModelAttribute String msg) {
        map.put("grades", GRADE.values());
        map.put("states", STATE.values());
        if (!StringUtils.isEmpty(msg)) {
            map.put(AppConstant.MSG, msg);
        }
        return "teacherform";
    }

    @PostMapping("/register")
    public String saveTeacher(@ModelAttribute Teacher teacher, RedirectAttributes redirectAttributes) {
        teacherService.save(teacher);
        redirectAttributes.addFlashAttribute(AppConstant.MSG, String.format(AppConstant.INSERT_SUCCESS_MSG, UserType.TEACHER.toString(), teacher.getName()));
        return "redirect:/teacher/register";
    }

    @GetMapping("/viewteachers")
    public String viewTeachers(@RequestParam(required = false, defaultValue = "1") int page, ModelMap map) {
        Page<Teacher> pageData = teacherService.findPaginatedPage(page);

        map.put("teachers", pageData.getContent());
        map.put("grades", GRADE.values());
        return "teacherslist";
    }

    @GetMapping("/filter")
    public String filterTeacher(@RequestParam(required = false, defaultValue = "") String grade,
                                @RequestParam(required = false, defaultValue = "") String subject,
                                ModelMap map) {

        GRADE gradeEnum;
        if (StringUtils.isEmpty(grade)) {
            gradeEnum = null;
        } else {
            gradeEnum = GRADE.valueOf(grade);
        }
        List<Teacher> teachers = teacherService.findBy(gradeEnum, subject);
        map.put("teachers", teachers);
        map.put("grades", GRADE.values());
        return "teacherslist";
    }

    @GetMapping("/{id}/delete")
    public String deleteTeacher(@PathVariable Long id) {
        teacherService.delete(id);
        return "redirect:/teacher/viewteachers";
    }

    @GetMapping("/edit")
    public String editTeacher(@RequestParam(required = false, defaultValue = "0") Long id) {

        return "redirect:/teacher/viewteachers";
    }
}
