package com.metahorizon.laliguras.user.controller;

import com.metahorizon.laliguras.core.constant.AppConstant;
import com.metahorizon.laliguras.core.enums.GRADE;
import com.metahorizon.laliguras.core.enums.STATE;
import com.metahorizon.laliguras.core.enums.UserType;
import com.metahorizon.laliguras.user.entity.Student;
import com.metahorizon.laliguras.user.entity.Teacher;
import com.metahorizon.laliguras.user.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.thymeleaf.util.StringUtils;

import java.util.List;

@Controller
@RequestMapping("/student")
public class StudentController{

    @Autowired
    private StudentService studentService;
    int pageValue = 0;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/register")
    public String getRegisterPage(ModelMap map, @ModelAttribute String msg) {
        map.put("grades", GRADE.values());
        map.put("states", STATE.values());
        if (!StringUtils.isEmpty(msg)) {
            map.put(AppConstant.MSG, msg);
        }
        return "studentform";
    }

    @PostMapping("/register")
    public String saveStudent(@ModelAttribute Student student, RedirectAttributes redirectAttributes) {
        studentService.save(student);
        redirectAttributes.addFlashAttribute(AppConstant.MSG, String.format(AppConstant.INSERT_SUCCESS_MSG, UserType.STUDENT.toString(), student.getName()));
        return "redirect:/student/register";
    }

    @GetMapping("/viewstudents")
    public String viewStudents(@RequestParam(required = false, defaultValue = "") String page, ModelMap map) {

        if(page.equals("next")){
            pageValue++;
        }
        if (page.equals("previous") && pageValue > 0){
            pageValue--;

        }

        Page<Student> pageData = studentService.findPaginatedPage(pageValue);
        map.put("students", pageData.getContent());
        map.put("grades", GRADE.values());
        return "studentslist";
    }

    @GetMapping("/filter")
    public String filterStudent(@RequestParam(required = false, defaultValue = "") String grade,
                                ModelMap map) {
        GRADE gradeEnum;
        if (StringUtils.isEmpty(grade)) {
            gradeEnum = null;
        } else {
            gradeEnum = GRADE.valueOf(grade);
        }
        List<Student> students = studentService.findBy(gradeEnum);
        map.put("students", students);
        map.put("grades", GRADE.values());
        return "studentslist";
    }
}
