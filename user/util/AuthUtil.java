package com.metahorizon.laliguras.user.util;

import com.metahorizon.laliguras.user.bean.UserDetailBean;
import com.metahorizon.laliguras.user.entity.User;
import lombok.experimental.UtilityClass;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;


@UtilityClass
public class AuthUtil {

    public static User getLoggedUser() {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object object = authentication.getPrincipal();
        if (object instanceof UserDetailBean) {
            UserDetailBean userDetailBean = (UserDetailBean) object;
            return userDetailBean.getUser();
        }


        return null;
    }
}
