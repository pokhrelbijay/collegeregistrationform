package com.metahorizon.laliguras.user.service;

import com.metahorizon.laliguras.user.bean.UserDetailBean;
import com.metahorizon.laliguras.user.entity.User;
import com.metahorizon.laliguras.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;



public class UserDetailServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {

        User user = userRepository.findByUserName(userName);
        if (null == user) {
            throw new UsernameNotFoundException("No record found under username " + userName);
        }
        return new UserDetailBean(user);
    }
}
