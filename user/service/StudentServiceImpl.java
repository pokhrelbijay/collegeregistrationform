package com.metahorizon.laliguras.user.service;

import com.metahorizon.laliguras.core.constant.AppConstant;
import com.metahorizon.laliguras.core.enums.GRADE;
import com.metahorizon.laliguras.core.enums.UserType;
import com.metahorizon.laliguras.random.password.NumUtil;
import com.metahorizon.laliguras.user.entity.Student;
import com.metahorizon.laliguras.user.entity.User;
import com.metahorizon.laliguras.user.repository.StudentRepository;
import com.metahorizon.laliguras.user.repository.UserRepository;
import com.metahorizon.laliguras.user.service.mail.Mail;
import com.metahorizon.laliguras.user.service.mail.MailService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;
    private final UserRepository userRepository;
    private final MailService mailService;
    private final PasswordEncoder passwordEncoder;


    @Override
    public Student save(Student student) {
        student = studentRepository.save(student);

        User user = new User();
        String sEmail = student.getEmail();
        user.setUserName(sEmail);
        user.setPassword(passwordEncoder.encode(AppConstant.DEFAULT_PASSWORD));
        user.setUserType(UserType.STUDENT);
        user.setAssociateId(student.getId());

        userRepository.save(user);

        NumUtil numUtil = new NumUtil();
        Mail mail = new Mail();
        mail.setMailFrom("dojocat2020@gmail.com");
        mail.setMailTo(sEmail);
        mail.setMailSubject("Important!!!");
        mail.setMailContent("Hello Student!" + "\n\nThis is to inform you about your password." + "\n\nPassword: "
                + numUtil.generate4DigitRandomNumber() + "\n\nWith regards," + "\nLaliguras Family");
        mailService.sendEmail(mail);
        return student;
    }

    @Override
    public List<Student> findAll() {
        return studentRepository.findAll();
    }

    @Override
    public void delete(Long id) {
        // TODO - delte logic implemenation for student
    }

    @Override
    public List<Student> findBy(GRADE grade, String subject) {
        return null;
    }

    @Override
    public List<Student> findBy(GRADE grade) {
        if (null == grade) {
            return studentRepository.findAll();
        } else {
            return studentRepository.findStudentByGrade(grade);
        }
    }

    @Override
    public Page<Student> findPaginatedPage(int page) {
        return studentRepository.findAll(PageRequest.of(page, AppConstant.RECORD_ROW_ON_PAGE));
    }
}
