package com.metahorizon.laliguras.user.service;

import com.metahorizon.laliguras.core.constant.AppConstant;
import com.metahorizon.laliguras.core.enums.GRADE;
import com.metahorizon.laliguras.core.enums.UserType;
import com.metahorizon.laliguras.random.password.NumUtil;
import com.metahorizon.laliguras.user.entity.Teacher;
import com.metahorizon.laliguras.user.entity.User;
import com.metahorizon.laliguras.user.repository.TeacherRepository;
import com.metahorizon.laliguras.user.repository.UserRepository;
import com.metahorizon.laliguras.user.service.mail.Mail;
import com.metahorizon.laliguras.user.service.mail.MailService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.util.StringUtils;

import java.util.List;

@Service
@AllArgsConstructor
public class TeacherServiceImpl implements TeacherService {

    private final TeacherRepository teacherRepository;
    private final UserRepository userRepository;
    private final MailService mailService;
    private final PasswordEncoder passwordEncoder;


    @Override
    public Teacher save(Teacher teacher) {
        teacher = teacherRepository.save(teacher);

        User user = new User();
        String tEmail = teacher.getEmail();
        user.setUserName(tEmail);
        user.setPassword(passwordEncoder.encode(AppConstant.DEFAULT_PASSWORD));
        user.setUserType(UserType.TEACHER);
        user.setAssociateId(teacher.getId());

        userRepository.save(user);

        NumUtil numUtil = new NumUtil();
        Mail mail = new Mail();
        mail.setMailFrom("dojocat2020@gmail.com");
        mail.setMailTo(tEmail);
        mail.setMailSubject("Important!!!");
        mail.setMailContent("Hello Teacher!" + "\n\n This is to inform you about your password." + "\n\nPassword: "
                + numUtil.generate4DigitRandomNumber() + "\n\nWith regards," + "\nLaliguras Family");
        mailService.sendEmail(mail);

        return teacher;
    }

    @Override
    public List<Teacher> findAll() {
        return teacherRepository.findAll();
    }

    @Override
    @Transactional
    public void delete(Long id) {
        teacherRepository.deleteById(id);
        userRepository.deleteAllByAssociateId(id);
    }

    @Override
    public List<Teacher> findBy(GRADE grade, String subject) {
        if (null == grade && StringUtils.isEmpty(subject)) {
            return teacherRepository.findAll();
        }

        if (null != grade && !StringUtils.isEmpty(subject)) {
            return teacherRepository.findTeacherByGradeAndSubject(grade, subject);
        }

        if (!StringUtils.isEmpty(subject)) {
            return teacherRepository.findTeacherBySubject(subject);

        }
        if (grade != null) {
            return teacherRepository.findTeacherByGrade(grade);
        }

        return null;
    }

    @Override
    public Page<Teacher> findPaginatedPage(int page) {
        return teacherRepository.findAll(PageRequest.of(page, AppConstant.RECORD_ROW_ON_PAGE));
    }

    @Override
    public List<Teacher> findBy(GRADE grade) {
        return null;
    }

}
