package com.metahorizon.laliguras.user.service;

import com.metahorizon.laliguras.core.service.BaseService;
import com.metahorizon.laliguras.user.entity.Teacher;
import org.springframework.data.domain.Page;

public interface TeacherService extends BaseService<Teacher> {

    Page<Teacher> findPaginatedPage(int page);
}
