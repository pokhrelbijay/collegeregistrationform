package com.metahorizon.laliguras.user.service;

import com.metahorizon.laliguras.core.service.BaseService;
import com.metahorizon.laliguras.user.entity.Student;
import org.springframework.data.domain.Page;


public interface StudentService extends BaseService<Student> {
    Page<Student> findPaginatedPage(int page);
        }
