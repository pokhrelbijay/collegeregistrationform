package com.metahorizon.laliguras.user.service.mail;


public interface MailService {

     void sendEmail(Mail mail);
}
