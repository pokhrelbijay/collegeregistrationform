package com.metahorizon.laliguras.user.bean;

import com.metahorizon.laliguras.core.enums.UserType;
import com.metahorizon.laliguras.user.entity.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;



public class UserDetailBean implements UserDetails {
    private final User user;

    public UserDetailBean(User user) {
        this.user = user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        if (user.getUserType().equals(UserType.ADMIN)) {
            AuthorityUtils.createAuthorityList("ROLE_ADMIN");
        }
        if (user.getUserType().equals(UserType.TEACHER)) {
            AuthorityUtils.createAuthorityList("ROLE_TEACHER");
        }
        if (user.getUserType().equals(UserType.STUDENT)) {
            AuthorityUtils.createAuthorityList("ROLE_STUDENT");
        }
        return AuthorityUtils.NO_AUTHORITIES;
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getUserName();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public User getUser() {
        return user;
    }
}
