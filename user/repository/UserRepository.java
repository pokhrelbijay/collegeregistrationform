package com.metahorizon.laliguras.user.repository;

import com.metahorizon.laliguras.user.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    void deleteAllByAssociateId(Long associateId);

    //    @Query("select User from User where userName=:email")
    User findByUserName(String email);


}
