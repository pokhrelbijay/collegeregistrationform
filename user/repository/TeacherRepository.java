package com.metahorizon.laliguras.user.repository;

import com.metahorizon.laliguras.core.enums.GRADE;
import com.metahorizon.laliguras.user.entity.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.security.auth.Subject;
import java.util.List;

public interface TeacherRepository extends JpaRepository<Teacher,Long> {
    List<Teacher> findTeacherByGradeAndSubject(GRADE grade, String subject);
    List<Teacher> findTeacherByGrade(GRADE grade);
    List<Teacher> findTeacherBySubject(String subject);
}
