package com.metahorizon.laliguras.user.repository;

import com.metahorizon.laliguras.core.enums.GRADE;
import com.metahorizon.laliguras.user.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
    List<Student> findStudentByGrade(GRADE grade);
}
