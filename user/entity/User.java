package com.metahorizon.laliguras.user.entity;

import com.metahorizon.laliguras.core.enums.UserType;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
@Data
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String userName;
    private String password;
    private UserType userType;
    private Long associateId;

}

