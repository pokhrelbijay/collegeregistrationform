package com.metahorizon.laliguras.webservice.dto;

import lombok.Builder;
import lombok.Data;

@Data
public class ResponseDto {

    private String message;
    private Object data;
    private boolean success;
//
//    public ResponseDto(String message) {
//        this.message = message;
//    }
//
//    public ResponseDto(String message, Object t) {
//        this.message = message;
//        this.t = t;
//    }
//
//    public ResponseDto(String message, Object t, boolean success) {
//        this.message = message;
//        this.t = t;
//        this.success = success;
//    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
