package com.metahorizon.laliguras.webservice.controller;

import com.metahorizon.laliguras.user.entity.Teacher;
import com.metahorizon.laliguras.user.service.TeacherService;
import com.metahorizon.laliguras.webservice.dto.ResponseDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/v1")
public class TeacherRestController {
    private final TeacherService teacherService;

    public TeacherRestController(TeacherService teacherService) {
        this.teacherService = teacherService;
    }

    @GetMapping("/teachers")
    public ResponseEntity<ResponseDto> getTeachers() {
        List<Teacher> teachers = teacherService.findAll();
        ResponseDto responseDto = new ResponseDto();
        responseDto.setData(teachers);
        responseDto.setMessage("Successfully Fetch Data.");
        responseDto.setSuccess(true);
        return ResponseEntity.ok(responseDto);
    }

    @PostMapping("teacher/add")
    public ResponseEntity<?> addTeacher(@RequestBody Teacher teacher) {
        ResponseDto responseDto = new ResponseDto();
        try {
            teacher = teacherService.save(teacher);
            responseDto.setMessage("Successfully saved on DB.");
            responseDto.setSuccess(true);
        } catch (Exception e) {
            responseDto.setMessage("Failed to save on DB. " + e.getMessage());
            responseDto.setSuccess(false);
        }
        responseDto.setData(teacher);

        return ResponseEntity.ok(responseDto);
    }

    @PutMapping("teacher/update")
    public ResponseEntity<?> updateTeacher(@RequestBody Teacher teacher) {
        ResponseDto responseDto = new ResponseDto();
        try {
            teacher = teacherService.save(teacher);
            responseDto.setMessage("Successfully updated on DB.");
            responseDto.setSuccess(true);
        } catch (Exception e) {
            responseDto.setMessage("Failed to save on DB. " + e.getMessage());
            responseDto.setSuccess(false);
        }
        responseDto.setData(teacher);

        return ResponseEntity.ok(responseDto);
    }

    @DeleteMapping("/teacher/{id}")
    public ResponseEntity<?> deleteTeacher(@PathVariable Long id) {
        teacherService.delete(id);
        ResponseDto responseDto = new ResponseDto();
        responseDto.setMessage("Successfully updated on DB.");
        responseDto.setSuccess(true);
        return ResponseEntity.ok(responseDto);
    }
}
