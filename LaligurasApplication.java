package com.metahorizon.laliguras;

import com.metahorizon.laliguras.core.constant.AppConstant;
import com.metahorizon.laliguras.core.enums.UserType;
import com.metahorizon.laliguras.user.entity.User;
import com.metahorizon.laliguras.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class LaligurasApplication {

    private final String SUPER_ADMIN = "admin@admin.com";
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public static void main(String[] args) {
        SpringApplication.run(LaligurasApplication.class, args);
    }

    @PostConstruct
    public void createSuperAdmin() {
        User superUser = userRepository.findByUserName(SUPER_ADMIN);
        if (null == superUser) {
            superUser = new User();
            superUser.setUserName(SUPER_ADMIN);
            superUser.setPassword(passwordEncoder.encode(AppConstant.DEFAULT_PASSWORD));
            superUser.setUserType(UserType.ADMIN);
            superUser.setAssociateId(0l);
            userRepository.save(superUser);
        }
    }
}
