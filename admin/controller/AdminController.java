package com.metahorizon.laliguras.admin.controller;

import com.metahorizon.laliguras.user.util.AuthUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/admin")
public class AdminController {

    @GetMapping("/dashboard")
    public String getAdminDashboard(ModelMap map) {
        map.put("loggedUser", AuthUtil.getLoggedUser());
        return "dashboard";
    }


}
