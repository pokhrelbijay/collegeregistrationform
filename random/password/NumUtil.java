package com.metahorizon.laliguras.random.password;

import java.util.Random;

public class NumUtil {

    public int generate4DigitRandomNumber() {
        Random random = new Random();
        String generatedPassword = String.format("%04d", random.nextInt(10000));
        return Integer.parseInt(generatedPassword);
    }
}